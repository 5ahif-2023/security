package at.htlstp.security.presentation;

import at.htlstp.security.persistence.IngredientRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.extras.springsecurity6.auth.Authorization;

import java.security.Principal;
import java.util.Map;

@Controller
public record IngredientController(IngredientRepository ingredientRepository) {

    @GetMapping("ingredients")
    public ModelAndView getAll() {
        var model = Map.of("ingredients", ingredientRepository.findAll());
        var viewName = "ingredient-list";
        return new ModelAndView(viewName, model);
    }

    @GetMapping("users")
    public void methodNeedsLoggedInUser(Principal principal) { // or Subinterface
        var username = principal.getName();
        // ...
    }
}