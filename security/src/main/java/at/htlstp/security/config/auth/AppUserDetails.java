package at.htlstp.security.config.auth;

import at.htlstp.security.domain.AppUser;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

@AllArgsConstructor
public class AppUserDetails implements UserDetails {

    private final AppUser user;

    // ROLE_ to differentiate between Roles and Authorities
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        var role = new SimpleGrantedAuthority("ROLE_" + user.getRole());
        return Set.of(role);
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }
}
