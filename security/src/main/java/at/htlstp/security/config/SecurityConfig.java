package at.htlstp.security.config;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import java.security.SecureRandom;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.Customizer.withDefaults;

/**
 * Entwicklung:
 * standard
 * standard mit eigenem User
 * basicAuth
 * rest - nur read geht
 * csrf disable
 * role
 * authority
 * preAuthorize
 * Cross Site Request Forgery
 * csrf disable nur für pure services ohne browser
 * basic -> formLogin
 * remember-me (Cookies löschen)
 */
@EnableWebSecurity
@Configuration
public class SecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        var passwordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());
        // System.out.println("pw: " + passwordEncoder.encode("pw"));
        // System.out.println("123: " + passwordEncoder.encode("123"));
        return passwordEncoder;
    }

    @Bean
    public AuthenticationProvider authenticationManager(
            PasswordEncoder passwordEncoder,
            UserDetailsService userDetailsService) {
        var authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        authenticationProvider.setUserDetailsService(userDetailsService);
        return authenticationProvider;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
        return apiBasicAuth(http);
    }


    private SecurityFilterChain standard(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
                .formLogin(formLogin -> formLogin.permitAll())
                .build();
    }

    private SecurityFilterChain free(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(auth -> auth.anyRequest().permitAll())
                .formLogin(AbstractHttpConfigurer::disable)
                .build();
    }

    private SecurityFilterChain usual(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(auth ->
                        auth.requestMatchers(GET, "/public/**").permitAll()
                                .requestMatchers("/admin/**").hasRole("admin")
                                .anyRequest().authenticated()
                )
                .formLogin(withDefaults())
                .build();
    }

    private SecurityFilterChain apiBasicAuth(HttpSecurity http) throws Exception {
        return http
                .csrf(AbstractHttpConfigurer::disable) // no generated views, no sessions
                .authorizeHttpRequests(auth ->
                        auth.requestMatchers("/", "/home").permitAll()
                                .requestMatchers("/admin/**").hasRole("admin")
                                .anyRequest().authenticated()
                )
                .httpBasic(withDefaults())
                .build();
    }

    private SecurityFilterChain custom(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(auth ->
                        auth.requestMatchers("/", "/home", "/register", "/styles/**").permitAll()
                                .requestMatchers(POST, "/register").permitAll()
                                .requestMatchers("/admin/**").hasRole("admin")
                                .anyRequest().authenticated()
                )
                .formLogin(config -> config
                        .loginPage("/login")
                        .permitAll()
                        .defaultSuccessUrl("/greeting", true)
                )
                .rememberMe(withDefaults())
                .build();
    }

    private SecurityFilterChain h2ConsoleEnabled(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
        var mvcMatcherBuilder = new MvcRequestMatcher.Builder(introspector);
        return http
                .csrf(csrf -> csrf
                        .ignoringRequestMatchers(PathRequest.toH2Console())
                )
                .headers(headers -> headers
                        .frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin)
                )
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(
                                mvcMatcherBuilder.pattern("/"),
                                mvcMatcherBuilder.pattern("/home"),
                                mvcMatcherBuilder.pattern("/register"),
                                mvcMatcherBuilder.pattern("/confirmRegistration/**"),
                                mvcMatcherBuilder.pattern("/error"),
                                mvcMatcherBuilder.pattern("/styles/**")
                        ).permitAll()
                        .requestMatchers(PathRequest.toH2Console()).permitAll()
                        .requestMatchers(mvcMatcherBuilder.pattern(POST, "/register")).permitAll()
                        .requestMatchers(mvcMatcherBuilder.pattern("/admin/**")).hasRole("admin")
                        .anyRequest().authenticated()
                )
                .formLogin(config -> config
                        .loginPage("/login")
                        .permitAll()
                        .defaultSuccessUrl("/greeting")
                )
                .build();
    }

    public SecurityFilterChain csrfApi(HttpSecurity http) throws Exception {
        return http
                .csrf(csrf -> csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()))
                .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
                .httpBasic(withDefaults())
                .build();
    }

    public SecurityFilterChain oAuth(HttpSecurity http) throws Exception {
        return http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        //.requestMatchers(new AntPathRequestMatcher("/token", "POST")).permitAll()
                        .anyRequest().authenticated()
                )
                .oauth2ResourceServer(oAuth -> oAuth.jwt(withDefaults()))
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .httpBasic(withDefaults())
                .build();
    }
}
