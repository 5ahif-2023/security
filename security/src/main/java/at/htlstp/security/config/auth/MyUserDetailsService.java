package at.htlstp.security.config.auth;

import at.htlstp.security.persistence.AppUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class MyUserDetailsService implements UserDetailsService {

    private AppUserRepository userRepository;

    /**
     * We use the email for login purposes.
     *
     * @param email the username identifying the user whose data is required.
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var user = userRepository
                .findById(email)
                .orElseThrow(() -> new UsernameNotFoundException("email"));
        return new AppUserDetails(user);
    }
}
