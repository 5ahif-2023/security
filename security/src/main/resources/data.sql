insert into ingredients(title)
values ('cheese'),
       ('tomatoes'),
       ('salami'),
       ('tuna'),
       ('onion'),
       ('garlic'),
       ('chili');

insert into pizzas(title,description)
values ('Margarita', 'Cheap, yummy'),
       ('Tuna', 'Save the planet?'),
       ('Palermo', 'Mafia was here');

insert into pizza_ingredients(pizza_id, ingredient_id)
values (1, 1),
       (1, 2),
       (2, 1),
       (2, 2),
       (2, 4),
       (2, 5),
       (3, 1),
       (3, 2),
       (3, 3),
       (3, 6),
       (3, 7);
INSERT INTO users(email, username, password, role, enabled)
VALUES ('alfred@some.where', 'alfred', '$2a$10$aCaLV4DSLHAuvHxZ3Q39jeU6pFywjORUxIKalCpnJqG3wKWzG0Uwi', 'user', true), /* pw*/
       ('bernd@some.where', 'bernd', '$2a$10$aCaLV4DSLHAuvHxZ3Q39jeU6pFywjORUxIKalCpnJqG3wKWzG0Uwi', 'user', true), /*pw*/
       ('admin@some.where', 'admin', '$2a$10$QTcgWuzaeR2naLZyeewp2uBUu9ZCN60ksx88Mg.KImHz8o/GfStUS', 'admin', true); /*123*/